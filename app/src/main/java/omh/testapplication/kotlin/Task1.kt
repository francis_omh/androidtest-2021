package omh.testapplication.kotlin

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import omh.testapplication.MApplication
import omh.testapplication.R

/* TODO:
    * Task 1) Optimus Prime!
    * --- Description ---
    * Optimize the view layout hierarchy and do whatever you can to make this activity launch faster on the 1st load
    * Please take note that the UI should look the same as before
    *
    * Edit activity_task1.xml and content_complex_view.xml
    *
    * */


class Task1 : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        val timeTaken = MApplication.getInstance().stopActivityTransitionTimer()
        AlertDialog.Builder(this@Task1)
                .setTitle("Completed")
                .setMessage("Time taken for activity to be launched: " + timeTaken + "ms")
                .setCancelable(false)
                .setPositiveButton("ok") { dialog, which -> dialog.dismiss() }.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task1)
        val toolbar = findViewById<Toolbar>(R.id.activity_task1_toolbar)
        setSupportActionBar(toolbar)

        val fab = findViewById<FloatingActionButton>(R.id.activity_task1_fab)
        fab.setOnClickListener(View.OnClickListener { view ->
            Snackbar.make(view, "Copyright © 2019 Ohmyhome Pte Ltd, All rights reserved", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        })
    }
}