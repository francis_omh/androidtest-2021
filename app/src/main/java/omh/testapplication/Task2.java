package omh.testapplication;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/* TODO:
    * Task 2) Photocopy machine!
    *
    *  --- Description ---
    *  Study the image shown in the activity, task2.png
    *  and replicate the design into layouts and views.
    *  There is no restriction on how to replicate the design use your
    *  creativity :)
    *
    *  --- Requirements ---
    * - Left/Right swipeable calendar
    * - Bottom slideable overlay + expandable listview
    * - Follow the UI design as closely as possible
    * - Naming conventions have to be clear and readable
    * - Code has to be neat and organize
    * - Create any custom files/classes you want, edit file activity_task2.xml
    * - Any missing assets please get from https://icons8.com/
    * - Incorrect icons/ images will not be penalised
    *
    * --- References ---
    * - Google Calendar
    *
    * --- Plus Points ---
    * - Good UX
    * - Using newer layouts, i.e. ConstraintLayout, CoordinatorLayout
    *
    * */


public class Task2 extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task2);

    }

}
